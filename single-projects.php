<?php get_header(); ?>

<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
		<h2 class="title"><?php the_title(); ?></h2>
		<div class="main"><?php the_content(); ?></div>
		<div class="side">
			
			<span class="projectcats"><a href="https://playstreet.com.au/projects/">Projects</a>: <?php $terms = get_the_terms( $post->ID , 'project-categories' ); 
				$i = 1;
                    foreach ( $terms as $term ) {
                        $term_link = get_term_link( $term, 'project-categories' );
                        if( is_wp_error( $term_link ) )
                        continue;
                    echo '<a href="' . $term_link . '">' . $term->name . '</a>';
                    echo ($i < count($terms))? " | " : "";
        // Increment counter
        $i++;
                    } 
            ?></span>
			
			<span class="project-details-title">Project Details</span>
			<?php
			if ( get_field( 'more_details' ) ) {
				?>
				<div class="project-detail"><?php the_field( 'more_details' ); ?></div><?php } ?>
		</div>
		<div class="gallery">
			<?php if ( have_rows( 'gallery' ) ) { ?>
				<div class="images">
					<?php
					$count = 0;
					while ( have_rows( 'gallery' ) ) {
						the_row();
						?>
						<a data-lightbox="groupy" href="<?php echo get_sub_field( 'image' )['url']; ?>" class="image">
							<img class="image-image" src="<?php echo wp_get_attachment_image_url( get_sub_field( 'image' )['id'], 'featured-image' ); ?>">
							<div class="image-details">
								<span class="image-caption"><?php echo get_sub_field( 'image' )['caption']; ?></span>
							</div>
						</a>
					<?php $count++; } ?>
				</div>
			<?php } ?>
		</div>
		<?php
	}
}
?>

<?php get_footer(); ?>
