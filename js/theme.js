;(function($) {
    $(document).ready(function() {
        'use strict'

        // MENU
        $('.toggle-navigation').click(function(e) {
            $('.primary-navigation')
                .add($(this))
                .toggleClass('active')
            $('body').toggleClass('nav-is-open')

            e.preventDefault()
        })

        $('.read-more-cutter').click(function(e) {
            $(this).addClass('is-active')
            e.preventDefault()
        })
    })
})(jQuery)
