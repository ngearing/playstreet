(function($) {
    var $slider = $('.slider')
    var slickOptions = {
        // cssEase: 'cubic-bezier(0.87, 0.03, 0.41, 0.9)',
        cssEase: 'ease-in-out',
        slide: '.slide',
        fade: true,
        speed: 1250,
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 4000,
        pauseOnHover: false,
        pauseOnFocus: false,
        slidesToShow: 1,
        slidesToScroll: 1
    }
    var videos = $slider.find('iframe')
    var players = []

    // Remove loading screen.
    // $slider.on('init', loaded)
    $slider.on('init', maybeLoadVideo)

    // Set page theme colors.
    $slider.on('beforeChange', setPageTheme)
    // Start slide transition before change so there is no jerk
    $slider.on('beforeChange', startSlideTransition)
    $slider.on('beforeChange', startVideo)

    $slider.on('afterChange', resetVideo)
    $slider.on('afterChange', endSlideTransition)

    setupVideos()
        .then(proms => console.log(proms))
        .then(() => {
            return startSlider()
        })
        .catch(e => {
            console.log(e)
            return false
        })

    async function setupVideos() {
        var proms = []
        Array.from(videos).forEach(vid => {
            var player = new Vimeo.Player(vid, {
                background: true
            })

            setPlayer(player)

            player.ready().then(() => {
                player.on('cuepoint', nextSlide)

                player
                    .getDuration()
                    .then(dur => {
                        player.duration = dur
                        return player
                    })
                    .then(player => {
                        player
                            .addCuePoint(player.duration * 0.9, {
                                event: 'nextSlide'
                            })
                            .then(cue => {
                                player.cue
                                    ? player.cue.push({
                                          id: cue,
                                          time: player.duration * 0.9
                                      })
                                    : (player.cue = [
                                          {
                                              id: cue,
                                              time: player.duration * 0.9
                                          }
                                      ])

                                return player
                            })
                            .catch(error => {
                                console.log(error)
                            })
                    })
                    .catch(error => {
                        console.log(error)
                    })
            })

            proms.push(player)
        })

        return Promise.all(proms)
    }

    function maybeLoadVideo(event, slick) {
        var firstSlide = $slider[0].querySelectorAll('.slide')[0]
        var video = firstSlide.querySelector('iframe')
        if (!video) {
            startSlideTransition(event, slick, false, 0)
            loaded($slider[0])
            return
        }

        var player = getPlayer(video.id)
        slickOptions.autoplay = false

        slick.slickPause()

        return player.play().then(function() {
            console.log('playing?')
            slick.slickPause()
            loaded($slider[0])
        })
    }

    function nextSlide(cuepoint) {
        console.log(cuepoint)
        if (cuepoint.data.event == 'nextSlide') {
            // Restart autoplay and go to next.
            $slider.slick('slickPlay')
            $slider.slick('slickNext')
        }
    }

    function startVideo(event, slick, current, next) {
        var nextElem = slick.$slides[next]
        var video = nextElem.querySelector('iframe')
        if (!video) {
            return
        }

        // Pause slick to play video up to cue point.
        // The manually change.
        slick.slickPause()

        var player = getPlayer(video.id)
        if (!player) {
            return
        }

        player.play()
    }

    function resetVideo(event, slick, current, next) {
        var currentElem = slick.$slides[current]
        var currentVideo = currentElem.querySelector('iframe')

        Array.from(videos).forEach(video => {
            if (video != currentVideo) {
                var player = getPlayer(video.id)
                if (!player) {
                    return
                }

                // Set video position back to start.
                player.getCurrentTime().then(time => {
                    if (time > 0) {
                        player.pause().then(() => {
                            player
                                .setCurrentTime(0)
                                .then(time => console.log(time))
                        })
                    }
                })
            }
        })
        slick.slickPlay()
    }

    async function startSlider() {
        console.log('starting slider')
        $slider.slick(slickOptions)
    }

    function loaded(elem) {
        if (elem) {
            elem.classList.add('loaded')
        } else {
            $(this).addClass('loaded')
        }
        console.log('loaded')
    }

    function setPageTheme(event, slick, current, next) {
        var nextElem = slick.$slides[next]
        var scheme =
            scheme ||
            Array.from(document.body.classList).filter(
                c => c.indexOf('scheme-') > -1
            )[0]
        var theme =
            theme ||
            Array.from(document.body.classList).filter(
                c => c.indexOf('theme-') > -1
            )[0]
        var newScheme = nextElem.getAttribute('data-scheme')
        var newTheme = nextElem.getAttribute('data-theme')

        document.body.classList.remove(scheme)
        document.body.classList.remove(theme)

        document.body.classList.add(newScheme)
        document.body.classList.add(newTheme)

        scheme = newScheme
        theme = newTheme
    }

    function startSlideTransition(event, slick, current, next) {
        slick.$slides[next].classList.add('zoom')
    }
    function endSlideTransition(event, slick, current) {
        var zoomed = Array.from(slick.$slides).filter(
            (a, k) => a.classList.contains('zoom') && k !== current
        )
        zoomed = Array.from(zoomed).forEach(z => z.classList.remove('zoom'))
    }

    function getPlayer(id) {
        var player = players.filter(p => p.element.id == id)

        if (player.length) {
            return player.pop()
        }

        return false
    }

    function setPlayer(player) {
        players.push(player)
    }
})(jQuery)
