;(function() {
    var videos = document.querySelectorAll('.videos iframe')
    const Player = function(elem) {
        var elem = elem
        var options = []
        var source = null

        return {
            elem,
            options,
            source,
            play: function() {},
            stop: function() {}
        }
    }

    Player.prototype.play = function() {}

    videos.forEach(vid => {
        console.log(vid)
        var player = new Player(vid)
        console.log(player)
        player.play()
    })
})()
