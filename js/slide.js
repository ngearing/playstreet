function Slider() {
	var slides = $('.slide');
	var titles = $('.slider-section--title');
	var pips = 	$('.pips .pip');
	var currentSlide = 0;
	var logo = $('.logo');

	$(titles).add(pips).click(function(e) {
		slideTo($(this).index());
		e.preventDefault();
	});

	window.setInterval(slideToNext, 4000);

	function slideToNext() {
		currentSlide++;
		if(currentSlide >= slides.length) {
			currentSlide = 0;
		}
		slideTo(currentSlide);
	}

	function slideTo(index) {
		var scheme = slides.eq(index).data('scheme');
		var type = slides.eq(index).data('type');

		$(logo)
		.removeClass('red green yellow dark light')
		.addClass(scheme + ' ' + type);

		$(slides)
		.add(titles)
		.removeClass('active');

		$(titles)
		.filter('[data-scheme="'+scheme+'"]')
		.addClass('active');

		slides.eq(index)
		.addClass('active');
	}
}