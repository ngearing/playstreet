<?php get_header(); ?>

<div class="main">
	<?php
	while ( have_posts() ) {
		the_post();
		?>

		<h1 class="page-title"><?php the_title(); ?></h1>

		<?php
		the_content();
	}
	?>
</div>

<div class="side">
	<?php
	wp_list_pages(
		array(
			'child_of' => 8,
			'depth' => 1,
			'title_li' => '<span><a href="#">About</a></span>',
		)
	);
	?>
</div>

<?php get_footer(); ?>
