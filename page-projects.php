<?php get_header(); ?>

<h2 class="hero-title">Projects</h2>

<a class="view-link" href="<?php echo home_url(); ?>/projects-all">View All Projects</a>
<div class="project-categories">
<?php
foreach (get_terms('project-categories') as $category) { ?>
    <a class="project-category" href="<?php echo get_term_link($category); ?>">
        <span class="project-category-title"><?php echo $category->name; ?></span>
        <div style="background-color: <?php the_field('type', $category); ?>;" class="filter-overlay"></div>
        <?php if (get_field('image', $category)) {
            echo wp_get_attachment_image(get_field('image', $category)['id'], 'featured-image');
        } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.png">
        <?php } ?>
    </a>

<?php }
?>
</div>

<?php get_footer(); ?>