<?php

$slides = get_field('slides', get_the_ID());

if ( ! $slides ) {
    slider();
    return;
}

wp_enqueue_script( 'slider' );

$output = '';
$output = get_transient( 'playst-home-slider' );
if ( $output ) {
    echo $output;
    return;
}

foreach ( $slides as $k => $slide ) :

    $type    = strtolower( $slide['asset_type'] );
    $content = $slide[ $type ];

    if ( ! $content ) {
        continue;
    }

    $slide['attr'] = [
        'data-theme'  => 'theme-' . strtolower( $slide['colour'] ),
        'data-type'   => strtolower( $slide['asset_type'] ),
    ];


    if ( $type == 'embed' ) {

        $content = url_to_iframe($content);

    } else {
        // Grab the image.
        $srcsets = [
            '(orientation: portrait)'  => [
                'slider_small_portrait',
                'slider_medium_portrait',
                'slider_large_portrait',
            ],
        ];

        $imageLandscape = $content['ID'] ?: $content;
        $imagePortrait = $slide['portrait_image'];
        if ( ! $imagePortrait ) {
            $imagePortrait = $imageLandscape;
        }

        $meta = wp_get_attachment_metadata( $imagePortrait );

        $srcset = [];
        foreach ( $srcsets as $media => $src ) {

            $sizes = [];

            foreach ( $src as $size ) {
                if ( ! isset( $meta['sizes'][ $size ] ) ) {
                    continue;
                }

                $sizes[] = sprintf(
                    '%s %sw',
                    wp_get_attachment_image_url( $imagePortrait, $size ),
                    $meta['sizes'][ $size ]['width']
                );
            }

            $srcset[] = sprintf( '<source media="%s" srcset="%s">', $media, implode( ', ', $sizes ) );
        }

        $content = sprintf(
            '<picture>
                %s
                %s
            </picture>',
            implode( '', $srcset ),
            wp_get_attachment_image( $imageLandscape, 'full' )
        );
    }

    $attrs = array_map( 'esc_attr', $slide['attr'] );
    $attrs = array_map(
        function( $v, $k ) {
                return "$k='$v'";
        },
        $attrs,
        array_keys( $attrs )
    );
    $attrs = rtrim( implode( ' ', $attrs ) );

    $output .= sprintf(
        '<div class="slide %1$s" %2$s>
            %3$s
            <div class="slide--attribute">%4$s</div>
            <div class="slide--description"><a href="%6$s">%5$s</a></div>
        </div>',
        $type,
        $attrs,
        $content,
        $slide['attribution'] ?: '',
        $slide['description'] ?: '',
        $slide['link']
    );
endforeach;

$output = "<div class='slider loading'>$output</div>";

set_transient( 'playst-home-slider', $output, DAY_IN_SECONDS );

echo $output;
