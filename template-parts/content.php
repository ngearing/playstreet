<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-image">
        <?php if (has_post_thumbnail()) { ?>
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
        <?php } else { ?>
            <!-- <div class="image-placeholder"></div> -->
        <?php } ?>
    </div>
    <div class="blog-content">
        <header class="entry-header">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <p class="entry-date"><?php echo get_the_date(); ?></p>
        </header><!-- .entry-header -->
    
        <div class="entry-content">
            <?php
            if (is_singular()) {
                the_content();
            } else {
                the_field('excerpt');
            }
            ?>
        </div><!-- .entry-content -->
    </div>
</article><!-- #post-## -->
