<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
get_header(); ?>

    <div id="primary" class="content-area">
        <?php if ( 8 == $post->post_parent ) { ?><div class="main"><?php } else { ?><div id="main" class="site-main" role="main"><?php } ?>

            <?php
            while (have_posts()) :
                the_post();

                get_template_part('template-parts/content', 'page');
            endwhile; // End of the loop.
            ?>

        </div><!-- #main -->
  
    
    
    	<?php if ( 8 == $post->post_parent ) { ?>
		    <div class="side">
			<?php
			wp_list_pages(
				array(
					'child_of' => 8,
					'depth' => 1,
					'title_li' => '<span><a href="#">About</a></span>',
				)
			);
			?>
			</div>
		<?php } ?>
		
		  </div><!-- #primary -->

<?php
get_footer();
