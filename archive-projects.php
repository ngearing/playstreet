<?php get_header(); ?>

<div class="projects">
	<?php
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			?>
			<a href="<?php the_permalink(); ?>" class="project">
				<?php
				if ( get_field( 'featured_image' ) ) {
					echo wp_get_attachment_image( get_field( 'featured_image' )['id'], 'featured-image' );
				} else {
					?>
					<div class="image-placeholder"></div>
				<?php } ?>
				<div class="project-details">
					<span class="project-title"><?php the_title(); ?></span>
					<span class="project-address"><?php the_field( 'address' ); ?></span>
				</div>
				</a>
			<?php
		}
	}
	?>
</div>

<?php get_footer(); ?>
