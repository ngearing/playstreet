<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
	</div><!-- #content -->

	<?php if ( is_page( 'contact' ) ) {
		$map = get_field('map_embed');
		if (! $map ) {
			$map = '<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2923.676915755572!2d147.3213193162868!3d-42.87966297915557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xaa6e758598b390c9%3A0x243c9425de5cf46c!2sPlaystreet!5e0!3m2!1sen!2sau!4v1554079493521!5m2!1sen!2sau" width="800" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
		}
		printf('<div class="contact-map">%s</div>', $map);
	} ?>

	</div><!-- .container -->

</div><!-- #page -->

<footer class="site-footer">
	<div class="wrap"><span class="copyright">&copy; 2015-<?php echo date( 'Y' ); ?> Playstreet<span class="copyright--location">Hobart</span></span><a href="#" class="small-logo"><img src="https://playstreet.com.au/images/small-logo.png"></a>
		<div class="social">

			<a target="_blank" href="https://www.facebook.com/pages/Playstreet/676080772409132?ref=ts&amp;fref=ts"><i class="fab fa-facebook-f"></i></a>			
			<a target="_blank" href="https://www.instagram.com/playstreet_studio/"><i class="fab fa-instagram"></i></a>
			<a target="_blank" href="https://au.linkedin.com/company/playstreet-pty-ltd"><i class="fab fa-linkedin"></i></a>

		</div>
	</div>

</footer>

<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/app.css?version=20210909">
<!-- <link rel="stylesheet" href="https://fontastic.s3.amazonaws.com/rFhZRsHMmjmdgwchRqRN8T/icons.css"> -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<script src="https://kit.fontawesome.com/ad36d78588.js" crossorigin="anonymous"></script>

<?php wp_footer(); ?>

</body>
</html>
