<?php
/**
 * ggstyle functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */
if ( ! function_exists( 'ggstyle_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ggstyle_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ggstyle, use a find and replace
		 * to change 'ggstyle' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ggstyle', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Add image sizes
		 */

		add_image_size( 'featured-image', 768, 484, true );
		add_image_size( 'staff-image', 500, 500, true );

		// Slider images.
		add_image_size( 'slider_small_portrait', 400, 700, true );
		add_image_size( 'slider_medium_portrait', 800, 1400, true );
		add_image_size( 'slider_large_portrait', 1200, 2100, true );
		add_image_size( 'slider_small_landscape', 700, 400, true );
		add_image_size( 'slider_medium_landscape', 1400, 800, true );
		add_image_size( 'slider_large_landscape', 2048, 1080, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary', 'ggstyle' ),
				'footer'  => esc_html__( 'Footer', 'ggstyle' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 * See https://developer.wordpress.org/themes/functionality/post-formats/
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
			)
		);

		/*
		 * Enable theme support Theme Logo
		 * See https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'     => 300,
				'width'      => 300,
				'flex-width' => true,
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'ggstyle_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'ggstyle_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ggstyle_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ggstyle_content_width', 640 );
}
add_action( 'after_setup_theme', 'ggstyle_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ggstyle_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'ggstyle' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'ggstyle' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'ggstyle_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ggstyle_scripts() {
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'colorbox-css', get_template_directory_uri() . '/css/colorbox.css' );
	wp_enqueue_style( 'ggstyle-style', get_stylesheet_directory_uri() . '/style.css', array(), '20211005' );
	wp_enqueue_style( 'joni-hacks', get_stylesheet_directory_uri() . '/css/joni-hacks.css', array(), '2.2' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'imagesloaded' );
	wp_enqueue_script( 'ggstyle-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'ggstyle-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_register_script( 'slick-slider', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), '', true );
	wp_register_script( 'vimeo-player', '//player.vimeo.com/api/player.js', array(), '', true );
	wp_register_script( 'slider', get_template_directory_uri() . '/js/bg-slider-video.js', array( 'jquery', 'slick-slider', 'vimeo-player' ), '0.1.0', true );


	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/js/theme.js', array( 'jquery' ), '0.1.0', true );
	wp_enqueue_style( 'lightbox-css', get_template_directory_uri() . '/lightbox/css/lightbox.css' );
	wp_enqueue_script( 'lightbox-js', get_template_directory_uri() . '/lightbox/js/lightbox.js' );
}
add_action( 'wp_enqueue_scripts', 'ggstyle_scripts' );

if ( !in_array( 'classic-editor/classic-editor.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    if( get_option('classic-editor-replace') == 'classic' && get_option('classic-editor-allow-users') == 'no') {
        remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );
        remove_action( 'admin_enqueue_scripts', 'wp_common_block_scripts_and_styles' );
    }
}

/**
 * Remove Admin Guff.
 */
function ggstyle_remove_menus() {
	remove_menu_page( 'edit-comments.php' );          // Comments
	remove_menu_page( 'link-manager.php' );           // Links
}
add_action( 'admin_menu', 'ggstyle_remove_menus' );


add_filter(
	'body_class',
	function( $classes ) {
		global $post;

		if ( $post && ! is_archive() ) {
			$classes[] = $post->post_name;
		}

		if ( is_front_page() ) {
			$classes[] = 'scheme-green';
			$classes[] = 'theme-light';
		} else {
			$classes[] = 'scheme-grey';
			$classes[] = 'theme-dark';
		}
		return $classes;
	}
);

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

function init() {
	register_post_type(
		'projects',
		array(
			'label'       => 'Projects',
			'labels'      => array(
				'name'          => 'Projects',
				'singular_name' => 'Project',
				'add_new_item'  => 'Add New Project',
				'edit_item'     => 'Edit Project',
				'new_item'      => 'New Project',
			),
			'public'      => true,
			'has_archive' => true,
			'menu_icon'   => 'dashicons-hammer',
			'taxonomies'  => array( 'project-categories' ),
			'rewrite'     => array(
				'slug' => 'projects-all',
			),
		)
	);

	register_taxonomy(
		'project-categories',
		'projects',
		array(
			'labels'            => array(
				'name'          => 'Project Categories',
				'singular_name' => 'Project Category',
				'add_new_item'  => 'Add New Project Category',
				'edit_item'     => 'Edit Project Category',
				'new_item'      => 'New Project Category',
			),
			'public'            => true,
			'show_admin_column' => true,
			'has_archive'       => true,
			'hierarchical'      => true,
			'rewrite'           => array(
				'slug' => 'project-category',
			),
		)
	);
}
add_action( 'init', 'init' );

function excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'excerpt_more' );

function excerpt_length( $length ) {
	return 100;
}
add_filter( 'excerpt_length', 'excerpt_length' );

function array_merge_alternating( $arrays = [], $chunks = 1 ) {
	$output = [];

	while ( count( $arrays ) > 0 ) {
		foreach ( $arrays as $k => &$array ) {
			if ( empty( $array ) ) {
				unset( $arrays[ $k ] );
				continue;
			}
			$output = array_merge( $output, array_splice( $array, 0, $chunks ) );
		}
	}

	return $output;
}


function url_to_iframe( $url ) {
	// Add some settings to iframe.
	$content = '';
	$url        = parse_url( $url );
	$url_path   = $url['path'];
	$url_path   = explode( '/', $url_path );
	$id         = array_pop( $url_path );
	$parameters = [];

	// Get query parameters.
	if ( isset( $url['query'] ) ) {
		parse_str( $url['query'], $parameters );
	}

	// Find video source.
	$src = array_filter(
		[ 'vimeo', 'youtube' ],
		function( $s ) use ( $url ) {
			return strpos( $url['host'], $s ) !== false;
		}
	);
	$src = array_pop( $src );

	// Check to see if video is coming from embeddable url.
	$base_urls = [
		'vimeo' => [
			'host' => 'player.vimeo.com',
			'path' => "/video/$id",
		],
	];

	if ( $url['host'] !== $base_urls[ $src ]['host'] ) {
		$url['host'] = $base_urls[ $src ]['host'];
		$url['path'] = $base_urls[ $src ]['path'];
	}

	// Default query parameters for videos.
	$default_settings = [
		'vimeo'   => [
			'background'  => 1,
			'title'       => 0,
			'byline'      => 0,
			'portrait'    => 0,
			'autoplay'    => 0,
			'autopause'   => 0,
			'playsinline' => 1,
			'loop'        => 0,
			'muted'       => 1,
		],
		'youtube' => [
			'autoplay'       => 1,
			'controls'       => 0,
			'disablekb'      => 1,
			'enablejsapi'    => 1,
			'fs'             => 0,
			'iv_load_policy' => 3,
			'loop'           => 1,
			'modestbranding' => 1,
			'playsinline'    => 1,
		],
	];

	$parameters   = wp_parse_args( $default_settings[ $src ], $parameters );
	$url['query'] = urldecode( http_build_query( $parameters ) );

	$url = build_url( $url );

	// $content  = "<link rel='preload' as='document' href='$url'>";
	$content .= "<iframe id='$id' src='$url' width='100%' height='auto'></iframe>";

	return "<div class='iframe-wrap'>$content</div>";
}

function slider() {
	wp_enqueue_script( 'slider' );

	$output = '';
	$output = get_transient( 'playst-home-slider' );
	if ( $output ) {
		echo $output;
		return;
	}

	$fields = [
		[
			'key'   => 'landscape_architecture',
			'title' => 'Landscape Architecture',
			'color' => 'green',
		],
		[
			'key'   => 'play_spaces',
			'title' => 'Play Spaces',
			'color' => 'red',
		],
		[
			'key'   => 'urban_design',
			'title' => 'Urban Design',
			'color' => 'yellow',
		],
	];

	$slides     = [];
	$categories = '';
	// Set some data for each slide.
	foreach ( $fields as $field ) {
		$f_slides = get_field( $field['key'] . '_slides' );

		if ( ! $f_slides ) {
			continue;
		}
		array_walk(
			$f_slides,
			function( &$slide, $k, $field ) {
				$slide['attr'] = [
					'data-scheme' => 'scheme-' . strtolower( $field['color'] ),
					'data-theme'  => 'theme-' . strtolower( $slide['colour'] ),
					'data-type'   => strtolower( $slide['asset_type'] ),
				];
			},
			$field
		);
		$slides[] = $f_slides;

		// Setup category display text.
		$categories .= sprintf(
			'<div class="slider-category %1$s %2$s">
				<h2>%3$s</h2>
			</div>',
			$field['key'],
			$field['color'],
			$field['title']
		);
	}

	// Reorder slides to be alternating.
	$slides = array_merge_alternating( $slides );

	$output = '';

	foreach ( $slides as $k => $slide ) :

		$type    = strtolower( $slide['asset_type'] );
		$content = $slide[ $type ];

		if ( ! $content ) {
			continue;
		}

		if ( $type == 'embed' ) {
			// Add some settings to iframe.
			$url        = $content;
			$url        = parse_url( $url );
			$url_path   = $url['path'];
			$url_path   = explode( '/', $url_path );
			$id         = array_pop( $url_path );
			$parameters = [];

			// Get query parameters.
			if ( isset( $url['query'] ) ) {
				parse_str( $url['query'], $parameters );
			}

			// Find video source.
			$src = array_filter(
				[ 'vimeo', 'youtube' ],
				function( $s ) use ( $url ) {
					return strpos( $url['host'], $s ) !== false;
				}
			);
			$src = array_pop( $src );

			// Check to see if video is coming from embeddable url.
			$base_urls = [
				'vimeo' => [
					'host' => 'player.vimeo.com',
					'path' => "/video/$id",
				],
			];

			if ( $url['host'] !== $base_urls[ $src ]['host'] ) {
				$url['host'] = $base_urls[ $src ]['host'];
				$url['path'] = $base_urls[ $src ]['path'];
			}

			// Default query parameters for videos.
			$default_settings = [
				'vimeo'   => [
					'background'  => 1,
					'title'       => 0,
					'byline'      => 0,
					'portrait'    => 0,
					'autoplay'    => 0,
					'autopause'   => 0,
					'playsinline' => 1,
					'loop'        => 0,
					'muted'       => 1,
				],
				'youtube' => [
					'autoplay'       => 1,
					'controls'       => 0,
					'disablekb'      => 1,
					'enablejsapi'    => 1,
					'fs'             => 0,
					'iv_load_policy' => 3,
					'loop'           => 1,
					'modestbranding' => 1,
					'playsinline'    => 1,
				],
			];

			$parameters   = wp_parse_args( $default_settings[ $src ], $parameters );
			$url['query'] = urldecode( http_build_query( $parameters ) );

			$url = build_url( $url );

			// $content  = "<link rel='preload' as='document' href='$url'>";
			$content .= "<iframe id='$id' src='$url' width='100%' height='auto'></iframe>";

			$content = "<div class='iframe-wrap'>$content</div>";
		} else {
			// Grab the image.
			$srcsets = [
				'(orientation: portrait) and (max-width: 920px)'  => [
					'slider_small_portrait',
					'slider_medium_portrait',
					'slider_large_portrait',
				],
			];
			$meta    = wp_get_attachment_metadata( $content['ID'] ?: $content );

			$srcset = [];
			foreach ( $srcsets as $media => $src ) {

				$sizes = [];

				foreach ( $src as $size ) {
					if ( ! isset( $meta['sizes'][ $size ] ) ) {
						continue;
					}

					$sizes[] = sprintf(
						'%s %sw',
						wp_get_attachment_image_url( $content['ID'] ?: $content, $size ),
						$meta['sizes'][ $size ]['width']
					);
				}

				$srcset[] = sprintf( '<source media="%s" srcset="%s">', $media, implode( ', ', $sizes ) );
			}

			$content = sprintf(
				'<picture>
					%s
					%s
				</picture>',
				implode( '', $srcset ),
				wp_get_attachment_image( $content['ID'] ?: $content, 'full' )
			);
		}

		$attrs = array_map( 'esc_attr', $slide['attr'] );
		$attrs = array_map(
			function( $v, $k ) {
					return "$k='$v'";
			},
			$attrs,
			array_keys( $attrs )
		);
		$attrs = rtrim( implode( ' ', $attrs ) );

		$output .= sprintf(
			'<div class="slide %1$s" %2$s>
				%3$s
				<div class="slide--attribute">%4$s</div>
				<div class="slide--description"><a href="%6$s">%5$s</a></div>
			</div>',
			$type,
			$attrs,
			$content,
			$slide['attribution'] ?: '',
			$slide['description'] ?: '',
			$slide['link']
		);
	endforeach;

	$output = "<div class='slider loading'>$output</div>";

	set_transient( 'playst-home-slider', $output, DAY_IN_SECONDS );

	echo $output;
}



add_action(
	'save_post',
	function( $post_id ) {

		$post = get_post( $post_id );

		if ( $post->post_name == 'home' ) {
			delete_transient( 'playst-home-slider' );
		}

	}
);


function build_url( array $parts ) {
	return ( isset( $parts['scheme'] ) ? "{$parts['scheme']}:" : '' ) .
		( ( isset( $parts['user'] ) || isset( $parts['host'] ) ) ? '//' : '' ) .
		( isset( $parts['user'] ) ? "{$parts['user']}" : '' ) .
		( isset( $parts['pass'] ) ? ":{$parts['pass']}" : '' ) .
		( isset( $parts['user'] ) ? '@' : '' ) .
		( isset( $parts['host'] ) ? "{$parts['host']}" : '' ) .
		( isset( $parts['port'] ) ? ":{$parts['port']}" : '' ) .
		( isset( $parts['path'] ) ? "{$parts['path']}" : '' ) .
		( isset( $parts['query'] ) ? "?{$parts['query']}" : '' ) .
		( isset( $parts['fragment'] ) ? "#{$parts['fragment']}" : '' );
}
