<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ggstyle
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Playstreet Urban Design">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

<link rel="icon" 
	  type="image/png" 
	  href="<?php echo get_stylesheet_directory_uri(); ?>/img/PLAYSTREETicon.png?id=2">
	  


<?php wp_head(); ?>

<?php if (is_user_logged_in()) : ?>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; height: calc(100% - 32px) !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; height: calc(100% - 46px) !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<?php endif; ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142296539-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-142296539-1');
</script>

</head>

<body <?php body_class(); ?>>
	<?php if ( get_field( 'background_image', get_queried_object() ) || get_field( 'featured_image', get_queried_object() ) ) { ?>
		<div data-parallax="scroll" data-image-src="<?php echo get_field( 'featured_image', get_queried_object() )['url']; ?>" class="background-image-container"></div>
	<?php } ?>
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'ggstyle' ); ?></a>
	<div class="page">
		<header class="site-header wrap">
			<a href="<?php echo home_url(); ?>" class="logo">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 105.6 54" enable-background="new 0 0 105.6 54" xml:space="preserve">
					<g>
						<path fill="#FFFFFF" d="M0,0.1v24.4h7.2v-5.6H10c7.1,0,11.3-3.5,11.3-9.5c0-5.9-4.1-9.3-11-9.3H0zM7.2,12.8V6.3h3c2.3,0,3.7,1.2,3.7,3.2c0,2.1-1.4,3.3-3.7,3.3H7.2z M42.2,18.3H30.6V0.1h-7.2v24.4h18.8V18.3z M62.1,21.1l1.3,3.5h7.7L61,0h-7.1L43.8,24.6h7.7l1.3-3.5H62.1z M60,15.2h-5.1l2.5-6.9L60,15.2z M84.1,17.9l9.6-17.8h-8l-5.2,9.6l-5.2-9.6h-8l9.6,17.8l0,6.7h7.3L84.1,17.9z" class="logo--text"></path>
						<path fill="#FFFFFF" d="M62.9,28.4c-5.9,0-10.1,3-10.1,8.2c0,3.9,2.3,6,7.2,7.2l4.1,1c1.6,0.4,1.9,0.9,1.9,1.5c0,0.9-0.8,1.5-2.6,1.5c-2.3,0-3.9-0.9-4.2-2.7h-7.3c0.8,6.8,6.5,8.8,11.4,8.8c6.5,0,10.7-3.1,10.7-8.2c0-4.4-3-6.2-7.2-7.2l-4-1c-1.3-0.3-2-0.7-2-1.7c0-1.1,1-1.7,2.5-1.7c1.9,0,2.8,1,3.1,2.4h7.3C73.1,29.8,66.7,28.4,62.9,28.4 M96.9,35.2V29H75.1v6.3h7.3v18.2h7.3V35.2H96.9z M101.9,54c2,0,3.7-1.5,3.7-3.5c0-2-1.7-3.5-3.7-3.5c-2,0-3.7,1.5-3.7,3.5C98.2,52.5,99.8,54,101.9,54" class="logo--text"></path>
					</g>
					<polygon fill="#FFFFFF" points="42.7,28.9 32,53.8 39.7,53.8 50.4,28.9 " class="logo--slash"></polygon>
				</svg>
			</a>
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<span class="menu-toggle-box">
						<span class="menu-toggle-inner"></span>
					</span>
				</button>
				<style>
					<?php if ( ! is_front_page() ) { ?>
						.logo--text {
							fill: <?php the_field( 'main_navigation_colour', get_queried_object() ); ?>!important;
						}
					<?php } ?>

					<?php if ( get_post_type() == 'projects' ) { ?>
						.background-image-container {
							width: 100%;
							height: 66vh;
							background-position: center;
							background-size: cover;
							background-image: url(<?php echo get_field( 'featured_image', get_queried_object() )['url']; ?>);
						}
					<?php } elseif ( get_post_type() == 'page' || get_post_type() == 'post' ) { ?>
						.background-image-container {
							background-size: 100% auto;
							background-image: url(<?php echo get_field( 'background_image', get_queried_object() )['url']; ?>);
							width: 100%;
							height: 100%;
						}
					<?php } ?>
					
					<?php if ( get_field( 'main_navigation_colour', get_queried_object() ) ) { ?>
					.main-navigation a {
						color: <?php echo get_field( 'main_navigation_colour', get_queried_object() ); ?>;
					}
					<?php } ?>
					
					.menu-toggle-inner, .menu-toggle-inner::before, .menu-toggle-inner::after {
						background-color: <?php echo get_field( 'main_navigation_colour' ); ?>
					}
				</style>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
					)
				);
				?>
			</nav><!-- #site-navigation -->
	  </header>

<?php
if ( is_front_page() ) {
	get_template_part('template-parts/sliders');
}
?>

	<?php if ( get_field( 'header_image' ) ) { ?>
		<div class="page-header">
			<img class="headimg" src="<?php echo get_field( 'header_image' )['url']; ?>">
		</div>
	<?php } ?>

	<?php if ( get_query_Var( 'taxonomy' ) == 'project-categories' ) { ?>
		<div class="project-header">
			<div class="wrap">
				<h2 class="project-header-title"><?php echo get_queried_object()->name; ?></h2>
				<p class="project-header-description"><?php the_field( 'description', get_queried_object() ); ?></p>
			</div>
		</div>
	<?php } ?>

	<div class="wrap">
