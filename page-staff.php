<?php get_header(); ?>

<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		the_content();
	}
}
?>

<div class="gallery">
	<?php if ( have_rows( 'staff' ) ) { ?>
		<div class="images">
			<?php
			while ( have_rows( 'staff' ) ) {
				the_row();
				?>
				<a class="image" href="<?php the_sub_field( 'page' ); ?>">
					<img class="image-image" src="<?php echo wp_get_attachment_image_url( get_sub_field( 'image' )['id'], 'staff-image' ); ?>">
					<h3 class="name"><?php the_sub_field( 'name' ); ?></h3>
					<p class="position"><?php the_sub_field( 'position' ); ?></p>
				</a>
			<?php } ?>
		</div>
	<?php } ?>
</div>

<?php get_footer(); ?>
